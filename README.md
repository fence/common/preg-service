# Authorization Service

This is an abstraction on top of the PREG (Pre REGistration) API.

## Getting started

To use this library, you need to have an application registered on
[Application Portal](https://application-portal.web.cern.ch). If you do not
know what this means, refer to the [CERN Authorization Service documentation](https://auth.docs.cern.ch/applications/application-configuration). Make sure the registered application has OIDC SSO registration. The application needs to be added to the ‘grappa‘ group fence-api-read_{experiment} where experiment is alice, atlas, cms or lhcb.

With your application registered, you will use the **Client ID** and **Client secret**.

The official PREG API documentation can be found on the following URLs:

- UI (TEST): https://testhrapi.cern.ch/hrapi/swagger-ui/index.html
- YAML downloadable as a file(TEST): https://testhrapi.cern.ch/hrapi/v3/api-docs.yaml
- JSON exposed as an endpoint(TEST): https://testhrapi.cern.ch/hrapi/v3/api-docs

## Usage

The PREG provider is created with application credentials.

```php
// Example of $inProduction. Adapt for your needs!

$preRegistrationProvider = PreRegistrationProvider::createWithAppCredentials(
    $clientId       = getenv("CLIENT_ID"),
    $clientName     = getenv("CLIENT_SECRET"),
    $experimentName = "lhcb",
    $inProduction   = getenv("ENVIRONMENT") === "PRODUCTION",
);
```

#### `PreRegistrationProvider::getPreRegistrations()`

Get experiment's pre registrations.

```php
$registrations = $preRegistrationProvider->getPreRegistrations();

echo($registrations[0]->personId());
echo($registrations[0]->documentId());

// Output:
// 123456
// 1234567
```

### Schemas
The pre registration schema and its available options are as follows:

```php
personId                    => int
documentId                  => int
participationExperiment     => string (example: LHCb)
personNatureOfEmployment    => string ([ ADMINISTRATION, ENGINEERING, PHYSICS, SUMMERSTUDENT, TECHNICAL, OTHER ])
personEducationLevel        => string ([ BSCUNDERGRADUATE, MSCDIPLOMA, OTHER, PHD ])
personStudyingStatus        => string ([ CONCLUDED, STUDYING ])
personSeniorStatus          => ?string ([ SENIOR, NOTSENIOR, NULL]) ## this is an optional field, meaning that NULL does not indicate if the person is a senior or not.
```