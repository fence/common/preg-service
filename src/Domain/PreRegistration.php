<?php

namespace Glance\PreRegistrationService\Domain;

use DateTime;
use DateTimeImmutable;
use JsonSerializable;

final class PreRegistration implements JsonSerializable
{
    private $personId;
    private $documentId;
    private $createdTime;
    private $personNatureOfEmployment;
    private $personEducationLevel;
    private $personStudyingStatus;
    private $personSeniorStatus;

    private function __construct(
        int $personId,
        int $documentId,
        DateTimeImmutable $createdTime,
        string $personNatureOfEmployment,
        string $personEducationLevel,
        string $personStudyingStatus,
        ?string $personSeniorStatus
    ) {
        $this->personId = $personId;
        $this->documentId = $documentId;
        $this->createdTime = $createdTime;
        $this->personNatureOfEmployment = $personNatureOfEmployment;
        $this->personEducationLevel = $personEducationLevel;
        $this->personStudyingStatus = $personStudyingStatus;
        $this->personSeniorStatus = $personSeniorStatus;
    }

    public static function fromArray(array $row): self
    {
        return new self(
            (int) $row["personId"],
            (int) $row["documentId"],
            new DateTimeImmutable($row["createdTime"]),
            $row["personNatureOfEmployment"],
            $row["personEducationLevel"],
            $row["personStudyingStatus"],
            $row["personSeniorStatus"]
        );
    }

    public function jsonSerialize(): array
    {
        return [
            "personId" => $this->personId,
            "documentId" => $this->documentId,
            "createdTime" => $this->createdTime->format('Y-m-d H:i'),
            "personNatureOfEmployment" => $this->personNatureOfEmployment,
            "personEducationLevel" => $this->personEducationLevel,
            "personStudyingStatus" => $this->personStudyingStatus,
            "personSeniorStatus" => $this->personSeniorStatus
        ];
    }

    public function personId(): int
    {
        return $this->personId;
    }

    public function documentId(): int
    {
        return $this->documentId;
    }

    public function createdTime(): DateTimeImmutable
    {
        return $this->createdTime;
    }

    public function personNatureOfEmployment(): string
    {
        return $this->personNatureOfEmployment;
    }

    public function personEducationLevel(): string
    {
        return $this->personEducationLevel;
    }

    public function personStudyingStatus(): string
    {
        return $this->personStudyingStatus;
    }

    public function personSeniorStatus(): ?string
    {
        return $this->personSeniorStatus;
    }
}
