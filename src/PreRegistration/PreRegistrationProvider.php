<?php

declare(strict_types=1);

namespace Glance\PreRegistrationService\PreRegistration;

use Glance\PreRegistrationService\RequestBuilder\PreRegistrationApi;
use Glance\PreRegistrationService\RequestBuilder\AuthorizationApi;
use Glance\PreRegistrationService\Domain\PreRegistration;
use GuzzleHttp\Client;

class PreRegistrationProvider
{
    private $client;
    private $authorizationApiClient;
    private $preRegistrationApiClient;

    final private function __construct(
        string $clientId,
        string $clientSecret,
        string $experimentName,
        bool $inProduction
    ) {
        $options = [
            "client" => new Client(),
            "authorizationApiClient" => AuthorizationApi::createWithAppCredentials(
                new Client(),
                $clientId,
                $clientSecret,
                $inProduction
            ),
            "preRegistrationApiClient" => PreRegistrationApi::createWithAppCredentials(
                new Client(),
                $experimentName,
                $inProduction
            ),
        ];

        $this->client = $options["client"];
        $this->authorizationApiClient = $options["authorizationApiClient"];
        $this->preRegistrationApiClient = $options["preRegistrationApiClient"];
    }

    public static function createWithAppCredentials(
        string $clientId,
        string $clientSecret,
        string $experimentName,
        bool $inProduction
    ): self {
        return new self(
            $clientId,
            $clientSecret,
            $experimentName,
            $inProduction
        );
    }

    public function getPreRegistrations(array $queryParams = []): array
    {
        $authorizationToken = $this->getAuthorizationToken();
        $response = $this->preRegistrationApiClient->get($authorizationToken, $queryParams);
        $responseBody = json_decode((string) $response->getBody(), true);

        $registrations = array_map(function (array $row) {
            return PreRegistration::fromArray($row);
        }, $responseBody);

        return $registrations;
    }

    private function getAuthorizationToken(): ?string
    {
        $response = $this->authorizationApiClient->post();
        $responseBody = json_decode((string) $response->getBody(), true);

        return $responseBody["access_token"];
    }
}
