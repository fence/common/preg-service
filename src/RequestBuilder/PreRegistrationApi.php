<?php

declare(strict_types=1);

namespace Glance\PreRegistrationService\RequestBuilder;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use DomainException;

class PreRegistrationApi
{
    private const TEST_DOMAIN = "https://testhrapi.cern.ch/hrapi/registration/api/v1/experiment/";
    private const PROD_DOMAIN = "https://hrapi.cern.ch/hrapi/registration/api/v1/experiment/";

    private $client;
    private $experimentName;
    private $inProduction;

    private function __construct(
        Client $client,
        string $experimentName,
        bool $inProduction
    ) {
        $this->client = $client;
        $this->experimentName = $experimentName;
        $this->inProduction = $inProduction;
    }

    private function endpoint(string $domain, string $queryString): string
    {
        return $domain . $this->experimentName . ($queryString ? "?" . $queryString : "");
    }

    public static function createWithAppCredentials(
        Client $client,
        string $experimentName,
        bool $inProduction
    ): self {
        return new self(
            $client,
            $experimentName,
            $inProduction
        );
    }

    public function get(string $authorizationToken, array $queryParams = []): ResponseInterface
    {
        $options = [
            "headers" => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer " . $authorizationToken
            ]
        ];
        try {
            return $this->client->get(
                $this->endpoint(
                    $this->inProduction ? self::PROD_DOMAIN : self::TEST_DOMAIN,
                    http_build_query($queryParams)
                ),
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }
    }
}
