<?php

declare(strict_types=1);

namespace Glance\PreRegistrationService\RequestBuilder;

use DomainException;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class AuthorizationApi
{
    private $client;
    private $clientId;
    private $clientSecret;
    private $inProduction;

    private const DOMAIN = "https://auth.cern.ch/auth/realms/cern/api-access/token";
    private const TEST_AUDIENCE = "testhrapi";
    private const PROD_AUDIENCE = "hrapi";

    private function __construct(
        Client $client,
        string $clientId,
        string $clientSecret,
        bool $inProduction
    ) {
        $this->client = $client;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->inProduction = $inProduction;
    }

    public static function createWithAppCredentials(
        Client $client,
        string $clientId,
        string $clientSecret,
        bool $inProduction = true
    ): self {
        return new self(
            $client,
            $clientId,
            $clientSecret,
            $inProduction
        );
    }

    private function getFormParams(): array
    {
        return [
            "grant_type" => "client_credentials",
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            "audience" => $this->inProduction ? self::PROD_AUDIENCE : self::TEST_AUDIENCE,
        ];
    }

    public function post(): ResponseInterface
    {
        $options = [
            "form_params" => array_merge($this->getFormParams()),
        ];
        try {
            return $this->client->post(
                self::DOMAIN,
                $options
            );
        } catch (\Throwable $th) {
            throw new DomainException("Error: " . $th);
        }
    }
}
