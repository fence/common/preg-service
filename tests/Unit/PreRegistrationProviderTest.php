<?php

declare(strict_types=1);

namespace Glance\PreRegistrationService\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Glance\PreRegistrationService\PreRegistration\PreRegistrationProvider;

final class PreRegistrationProviderTest extends TestCase
{
    public function testGetPreRegistrations(): void
    {
        $this->markTestSkipped('This test relies on the broken https://testhrapi.cern.ch/hrapi/registration/api/v1/experiment/ api');
        $preRegistrationProvider = PreRegistrationProvider::createWithAppCredentials(
            $clientId = getenv("CLIENT_ID"),
            $clientName = getenv("CLIENT_SECRET"),
            $experimentName = getenv("EXPERIMENT_NAME"),
            $inProduction = false
        );

        $registrations = $preRegistrationProvider->getPreRegistrations();
        $this->assertNotNull($registrations);
    }
}
